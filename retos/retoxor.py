def get_key(c1,m1):
    k0 = ""
    for elem in range(len(c1)):
      code = c1[elem]^m1[elem]
      k0 += str(code)
    return k0

def main():
    c1=b'00000101000000000001111000000000'
    c2=b'00011000000011100001100000001110'
    m1=b'01101101011011110111001001100001'
    k0 =b'01101000011011110110110001100001'
    m2=get_key(k0,c2)
    print("M2",m2)

if __name__==main():
    main()