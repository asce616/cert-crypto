import sys
from struct import unpack

def read_File(path):
   input_file = open(path, "rb")
   new_file = input_file.read()
   cuarentine = bytearray()
   cuarentine5A = unpack("<L", new_file[0:4])[0]
   
   for bits in new_file[cuarentine5A:]:
      cuarentine.append(bits ^ 0x5A)
   
   size = unpack("<Q", cuarentine[24:32])[0]
   index = size + 117
   
   final_file = bytearray()
   while index < len(cuarentine):
      size_aux = unpack("<L", cuarentine[index + 1:index + 5])[0]
      final = index + 5 + size_aux
      data = cuarentine[index + 5 : final]
      for b in data:
               final_file.append(b ^ 0x5A ^ 0xA5)
      index += 5 + size_aux
   input_file.close()
   return final_file
   

def create_File(path,file):
   with open(path, "wb") as f:
      f.write(bytes(file))
   f.close()

def main():
   if (len(sys.argv) != 3):
      print("debes de poner la ruta al archivo y el nombre del nuevo archivo ejemplo:\n python cipher.py algo.vbn nuevoarchivo")
      quit()
   file = sys.argv[1]
   name = sys.argv[2]

   decipher = read_File(file)
   create_File(name, decipher)
   


if __name__=="__main__":
    main()