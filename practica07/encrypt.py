#!/usr/bin/env python3

# Created by 
# Paulo Contreras Flores
# paulo.contreras.flores@gmail.com

# Modify by 
# Jorge Hernández Chavez
# argenis616@ciencias.unam.mx

from json.tool import main
from Crypto.Cipher import AES
import binascii, os, base64, mysql.connector, SecureString,scrypt, json

# Abrimos los archivos, los datos de coneccion que se encuentrar en un archivo json 
# Asi como la contraseña

def contrasenia():
  f = open('passwords.json')
  data = json.load(f)
  data = list(data.values())
  passwordc = data[0]
  f.close()
  return passwordc

def encrypt(name,diagnosis,treatment,passwordc):
    
  # El algoritmo de derivacion de llaves PBKDF2 necesita
  # una salt, se genera una salt pseudorandon de 16 bytes, 
  # el min requerido son 8 bytes, y el max de 16 bytes
  kdfSalt = os.urandom(16)

  # Algoritmo de derivacion de llaves PBKDF2, se toman
  # 32 bytes o 256 bits. Entonces se va a cifrar AES con
  # una llave de 256 bits, o AES-256
  secretKey = scrypt.hash(passwordc, kdfSalt, N=16384, r=8, p=1, buflen=32)

  # El modo de operacion GCM necesita un nonce (number
  # once) o mejor conocido como vector de inicializacion
  # o IV (initialization vector), de 256 bits para este
  # ejemplo. Usamos el que crea el mismo AES, Ademas de indicarle
  # que usaremos el modeo GCM.
  # Tambien necesitaremos de la etiqueta mac ya que esta indicara
  # Si se debe de confiar en el mensaje o no.


  aesCipherD = AES.new(secretKey, AES.MODE_GCM)
  nonceD = aesCipherD.nonce
  ciphertextD, authTagD = aesCipherD.encrypt_and_digest(diagnosis)

  aesCipherT = AES.new(secretKey, AES.MODE_GCM)
  nonceT = aesCipherT.nonce
  ciphertextT, authTagT = aesCipherT.encrypt_and_digest(treatment)


  #Procedemos a mostrar todos los datos, en bytes.
  print('Password:', passwordc)
  print('Diagnosis:', diagnosis)
  print('Medical treatment:', treatment)
  print('kdfSalt:', binascii.hexlify(kdfSalt))
  print('nonceD:', binascii.hexlify(nonceD))
  print('nonceT:', binascii.hexlify(nonceT))
  print('authTagD:', binascii.hexlify(authTagD))
  print('authTagT:', binascii.hexlify(authTagT))
  print('Diagnosis encrypted:', binascii.hexlify(ciphertextD))
  print('Medical treatment encrypted:', binascii.hexlify(ciphertextT))

  # Codificamos Todos los datos que iran en la base de datos.
  # Posteriormente codificamos para poder guardarlos en la base.
  kdfSalt = base64.b64encode(kdfSalt)
  nonceD = base64.b64encode(nonceD)
  nonceT = base64.b64encode(nonceT)
  authTagD = base64.b64encode(authTagD)
  authTagT = base64.b64encode(authTagT)
  ciphertextD = base64.b64encode(ciphertextD)
  ciphertextT = base64.b64encode(ciphertextT)

  print('kdfSalt (base64):', kdfSalt)
  print('nonceD (base64):', nonceD)
  print('nonceT (base64):', nonceT)
  print('authTagD (base64):', authTagD)
  print('authTagT (base64):', authTagT)
  print('ciphertextD (base64):', ciphertextD)
  print('ciphertextT (base64):', ciphertextT)
  coneccionB(name, ciphertextD, ciphertextT, kdfSalt, nonceD,nonceT,authTagD,authTagT)
  
  # Una vez terminando de desencriptar todos los datos de la base, procedemos a eleminar de la memoria ram los datos.
  SecureString.clearmem(passwordc)
  SecureString.clearmem(diagnosis)
  SecureString.clearmem(treatment)
  SecureString.clearmem(secretKey)

  # Vemos que efectivamente la memoria fue limpiada.
  print('AES encryption key:', binascii.hexlify(secretKey))
  print('Password:', passwordc)
  print('Diagnosis:', diagnosis)
  print('Treatment:', treatment)

#Creamos la coneccion a la base de datos
def coneccionB(name, ciphertextD, ciphertextT, kdfSalt, nonceD,nonceT,authTagD,authTagT):
  f = open('conn.json')
  data = json.load(f)
  data = list(data.values())
  user=data[0]
  password=data[1]
  host=data[2]
  port=data[3]
  database=data[4]
  f.close()
  try:
      mydb = mysql.connector.connect(user=user,
                                  password=password,
                                  host=host,
                                  port=port,
                                  database=database)
      cursor = mydb.cursor()
      insert_query = """INSERT INTO expediente (nombre, diagnostico, tratamiento, kdfSalt, nonceD,nonceT, authTagD,authTagT) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"""
      record_to_insert = (name, ciphertextD, ciphertextT, kdfSalt, nonceD,nonceT,authTagD,authTagT)
      cursor.execute(insert_query, record_to_insert)

      mydb.commit()
      count = cursor.lastrowid

      print("Record inserted successfully with id ", count)


  except mysql.connector.Error as err:
    print("Something went wrong: {}".format(err))


  finally:
      if mydb:
          cursor.close()
          mydb.close()
          print("DBMS connection is closed")



def main():
  name = "Jhon Connor"
  diagnosis = b'Heridas por T-800'
  treatment = b'Paracetamol cada 8 hrs'
  paswordc = contrasenia()
  encrypt(name,diagnosis,treatment,paswordc)


if __name__=='__main__':
  main()