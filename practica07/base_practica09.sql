CREATE DATABASE IF NOT EXISTS practica09;
USE practica09;
CREATE TABLE IF NOT EXISTS expediente (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  diagnostico varchar(450) NOT NULL,
  tratamiento varchar(450) NOT NULL,
  kdfSalt varchar(24) NOT NULL,
  nonceD varchar(256) NOT NULL,
  nonceT varchar(256) NOT NULL,
  authTagD varchar(256) NOT NULL,
  authTagT varchar(256) NOT NULL
);
CREATE USER IF NOT EXISTS 'cripto'@'localhost' IDENTIFIED BY 'mypassword';
GRANT ALL PRIVILEGES ON practica09.* TO 'cripto'@'localhost';
FLUSH PRIVILEGES;