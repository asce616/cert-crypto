#!/usr/bin/env python3

# Created by 
# Paulo Contreras Flores
# paulo.contreras.flores@gmail.com

# Modify by 
# Jorge Hernández Chavez
# argenis616@ciencias.unam.mx

from Crypto.Cipher import AES
import binascii, os, base64, mysql.connector, SecureString,scrypt, json


# Abrimos los archivos, los datos de coneccion que se encuentrar en un archivo json 
# Asi como la contraseña

def contrasenia():
    f = open('passwords.json')
    data = json.load(f)
    data = list(data.values())
    passwordc = data[0]
    f.close()
    return passwordc
    

#Creamos la coneccion a la base de datos
def coneccionB():
    f = open('conn.json')
    data = json.load(f)
    data = list(data.values())
    user=data[0]
    password=data[1]
    host=data[2]
    port=data[3]
    database=data[4]
    f.close()
    try:
        mydb = mysql.connector.connect(user=user,
                                    password=password,
                                    host=host,
                                    port=port,
                                    database=database)
        cursor = mydb.cursor()
        cursor.execute("SELECT * from expediente")
        data = cursor.fetchall()
        return data

    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))

    finally:
        if mydb:
            cursor.close()
            mydb.close()
            print("DBMS connection is closed")

def decrypt(data,passwordc):
    # Iteramos sobre todos los datos en la base de datos
    for dato in data:
        # Mostramos los datos que obtuvimos directamente de la base de datos.
        print("data",{
            'Id:': dato[0],
            'Name:': dato[1],
            'Diagnosis (base64):': dato[2],
            'Medical treatment (base64):': dato[3],
            'kdfSalt': dato[4],
            'nonceD': dato[5],
            'nonceT': dato[6],
            'authTagD':dato[7],
            'authTagT':dato[8],
            })

        # Decodificamos los datos anteriores. Ya que en un principio los codificamos en base 64
        kdfSalt = base64.b64decode(dato[4])
        nonceD = base64.b64decode(dato[5])
        nonceT = base64.b64decode(dato[6])
        authTagD = base64.b64decode(dato[7])
        authTagT = base64.b64decode(dato[8])
        ciphertextD = base64.b64decode(dato[2])
        ciphertextT = base64.b64decode(dato[3])

        #Mostramos los datos reales
        print('kdfSalt:', binascii.hexlify(kdfSalt))
        print('nonceD:', binascii.hexlify(nonceD))
        print('nonceT:', binascii.hexlify(nonceT))
        print('authTagD:', binascii.hexlify(authTagD))
        print('authTagT:', binascii.hexlify(authTagT))
        print('Diagnosis encrypted:', binascii.hexlify(ciphertextD))
        print('Medical treatment encrypted:', binascii.hexlify(ciphertextT))


        # Comenzamos a desencriptar con el metodo GCM de AES
        secretKey = scrypt.hash(passwordc, kdfSalt, N=16384, r=8, p=1, buflen=32)
        aesCipherD = AES.new(secretKey, AES.MODE_GCM, nonceD)
        plaintextD = aesCipherD.decrypt_and_verify(ciphertextD, authTagD)

        aesCipherT = AES.new(secretKey, AES.MODE_GCM, nonceT)
        plaintextT = aesCipherT.decrypt_and_verify(ciphertextT, authTagT)

        # Mostramos Los textos sin decodificar
        print('Diagnosis:', plaintextD)
        print('Medical treatment:', plaintextT)

    # Una vez terminando de desencriptar todos los datos de la base, procedemos a eleminar de la memoria ram los datos.
    SecureString.clearmem(passwordc)
    SecureString.clearmem(plaintextD)
    SecureString.clearmem(plaintextT)
    SecureString.clearmem(secretKey)

    # Vemos que efectivamente la memoria fue limpiada.
    print('AES encryption key:', binascii.hexlify(secretKey))
    print('Password:', passwordc)
    print('Diagnosis:', binascii.hexlify(plaintextD))
    print('Treatment:', binascii.hexlify(plaintextT))

def main():
    password = contrasenia()
    data = coneccionB()
    decrypt(data,password)


if __name__=='__main__':
  main()
    